package com.example.aiproject;

import java.util.ArrayList;
import java.util.List;

public class state {
    boolean giliran_player_1;
    String message;
    List<hewan> list_hewan_player1;
    List<hewan> list_hewan_player2;
    String[][] map;

    public state(boolean giliran_player_1) {
        this.giliran_player_1 = giliran_player_1;
        this.message = "";

        //init player1
        this.list_hewan_player1 = new ArrayList<>();
        list_hewan_player1.add(new hewan(6, 6, 1, false, true, "B")); //tikus
        list_hewan_player1.add(new hewan(1, 7, 2, false, true, "B")); //kucing
        list_hewan_player1.add(new hewan(5, 7, 3, false, true, "B")); //anjing
        list_hewan_player1.add(new hewan(2, 6, 4, false, true, "B")); //serigala
        list_hewan_player1.add(new hewan(4, 6, 5, false, true, "B")); //ceetah
        list_hewan_player1.add(new hewan(0, 8, 6, false, true, "B")); //harimau
        list_hewan_player1.add(new hewan(6, 8, 7, false, true, "B")); //singa
        list_hewan_player1.add(new hewan(0, 6, 8, false, true, "B")); //gajah

        //init player2
        this.list_hewan_player2 = new ArrayList<>();
        list_hewan_player2.add(new hewan(0, 2, 1, false, true, "R")); //tikus
        list_hewan_player2.add(new hewan(5, 1, 2, false, true, "R")); //kucing
        list_hewan_player2.add(new hewan(1, 1, 3, false, true, "R")); //anjing
        list_hewan_player2.add(new hewan(4, 2, 4, false, true, "R")); //serigala
        list_hewan_player2.add(new hewan(2, 2, 5, false, true, "R")); //ceetah
        list_hewan_player2.add(new hewan(6, 0, 6, false, true, "R")); //harimau
        list_hewan_player2.add(new hewan(0, 0, 7, false, true, "R")); //singa
        list_hewan_player2.add(new hewan(6, 2, 8, false, true, "R")); //gajah

        //init map
        this.map = new String[9][7];
        isi_map();
    }

    public state(state state) {
        //clone
        this.giliran_player_1 = state.giliran_player_1;
        this.message = state.message;

        list_hewan_player1 = new ArrayList<>();
        list_hewan_player2 = new ArrayList<>();

        for (int i = 0; i < 8; i++)
        {
            list_hewan_player1.add(new hewan(state.list_hewan_player1.get(i).x, state.list_hewan_player1.get(i).y, state.list_hewan_player1.get(i).rank, state.list_hewan_player1.get(i).rope, state.list_hewan_player1.get(i).status_hidup, state.list_hewan_player1.get(i).warna));
            list_hewan_player2.add(new hewan(state.list_hewan_player2.get(i).x, state.list_hewan_player2.get(i).y, state.list_hewan_player2.get(i).rank, state.list_hewan_player2.get(i).rope, state.list_hewan_player2.get(i).status_hidup, state.list_hewan_player2.get(i).warna));
        }

        this.map = new String[9][7];
        for (int i = 0; i < 9; i++)
        {
            for(int j = 0; j < 7; j++)
            {
                map[i][j] = state.map[i][j];
            }
        }
    }

    public void isi_map() {
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                map[i][j] = "T";
            }
        }

        map[8][3] = "BG";
        map[8][2] = "BO";
        map[8][4] = "BO";
        map[7][3] = "BO";

        map[0][3] = "RG";
        map[0][2] = "RO";
        map[0][4] = "RO";
        map[1][3] = "RO";

        map[3][1] = "A";
        map[3][2] = "A";
        map[4][1] = "A";
        map[4][2] = "A";
        map[5][1] = "A";
        map[5][2] = "A";

        map[3][4] = "A";
        map[3][5] = "A";
        map[4][4] = "A";
        map[4][5] = "A";
        map[5][4] = "A";
        map[5][5] = "A";

        for (int i = 0; i < 8; i++)
        {
            if (list_hewan_player1.get(i).status_hidup == true)
            {
                map[list_hewan_player1.get(i).y][list_hewan_player1.get(i).x] = "B" + list_hewan_player1.get(i).rank;
            }

            if (list_hewan_player2.get(i).status_hidup == true)
            {
                map[list_hewan_player2.get(i).y][list_hewan_player2.get(i).x] = "R" + list_hewan_player2.get(i).rank;
            }
        }
    }

    public void gerakkan_hewan_ke(hewan pemakan, int x, int y, String[][] terrain) {
        hewan dimakan = cari_hewan_di_posisi(x, y);

        if (dimakan != null) //jika ada hewan
        {
            dimakan.status_hidup = false;
        }

        if (terrain[y][x].length() > 1 && terrain[y][x].substring(1, 2).equals("O")) //jika ke rope
        {
            if (!terrain[y][x].substring(0, 1).equals(pemakan.warna))
            {
                pemakan.rope = true;
            }
        }
        else
        {
            pemakan.rope = false;
        }

        pemakan.x = x;
        pemakan.y = y;
        isi_map();
    }

    public boolean cek_valid_gerakkan_hewan_ke(hewan pemakan, int x, int y, String[][] terrain) {
        boolean valid_out_of_bounds = false; //cek out of bounds

        if (x >= 0 && x <= 6 && y >= 0 && y <= 8)
        {
            valid_out_of_bounds = true;
        }

        if (valid_out_of_bounds == true)
        {
            hewan dimakan = cari_hewan_di_posisi(x, y);

            boolean valid_gerak_atas_kanan_bawah_kiri = false; //cek valid gerakkan menuju atas kanan bawah kiri

            if ((pemakan.x == x && (pemakan.y - 1) == y) ||
                    (pemakan.x == x && (pemakan.y + 1) == y) ||
                    ((pemakan.x - 1) == x && pemakan.y == y) ||
                    ((pemakan.x + 1) == x && pemakan.y == y))
            {
                valid_gerak_atas_kanan_bawah_kiri = true;
            }

            if (valid_gerak_atas_kanan_bawah_kiri == true) //jika gerakkan menuju atas kanan bawah kiri
            {
                if (dimakan != null) //jika ada hewan
                {
                    if (pemakan.warna.equals(dimakan.warna)) //cek kanibal
                    {
                        message = "Cannibal";
                    }
                    else
                    {
                        if (dimakan.rope == true) //jika lawan di rope
                        {
                            return true;
                        }
                        else
                        {
                            if (pemakan.rank >= dimakan.rank) //jika rank lebih besar atau sama
                            {
                                if (pemakan.rank == 1) //pengecekan untuk tikus
                                {
                                    if (terrain[pemakan.y][pemakan.x].equals("A") && !terrain[dimakan.y][dimakan.x].equals("A")) //cek tidak bisa makan langsung saat keluar air
                                    {
                                        message = "Invalid Move";
                                    }
                                    else if (!terrain[pemakan.y][pemakan.x].equals("A") && terrain[dimakan.y][dimakan.x].equals("A")) //cek tidak bisa makan langsung saat masuk air
                                    {
                                        message = "Invalid Move";
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                                else
                                {
                                    if (terrain[dimakan.y][dimakan.x].equals("A")) //jika hewan makan hewan lain di air
                                    {
                                        message = "Invalid Move";
                                    }
                                    else
                                    {
                                        if (pemakan.rank == 8 && dimakan.rank == 1) //gajah tidak makan tikus
                                        {
                                            message = "Invalid Move";
                                        }
                                        else
                                        {
                                            return true;
                                        }
                                    }
                                }
                            }
                            else if (pemakan.rank == 1 && dimakan.rank == 8 && !terrain[pemakan.y][pemakan.x].equals("A")) //tikus makan gajah
                            {
                                return true;
                            }
                            else
                            {
                                message = "Invalid Move";
                            }
                        }
                    }
                }
                else
                {
                    if (terrain[y][x].substring(0, 1).equals("T")) //jika ke tanah
                    {
                        return true;
                    }
                    else if (terrain[y][x].substring(0, 1).equals("A")) //jika ke air
                    {
                        if (pemakan.rank == 1)
                        {
                            return true;
                        }
                        else
                        {
                            message = "Invalid Move";
                        }
                    }
                    else if (terrain[y][x].substring(1, 2).equals("O")) //jika ke rope
                    {
                        return true;
                    }
                    else if (terrain[y][x].substring(1, 2).equals("G")) //jika ke goal
                    {
                        if (terrain[y][x].substring(0, 1).equals("R") && !terrain[y][x].substring(0, 1).equals(pemakan.warna))
                        {
                            return true;
                        }
                        else if (terrain[y][x].substring(0, 1).equals("B") && !terrain[y][x].substring(0, 1).equals(pemakan.warna))
                        {
                            return true;
                        }
                        else
                        {
                            message = "Invalid Move";
                        }
                    }
                }
            }
            else if (pemakan.rank == 6 || pemakan.rank == 7) //siapa tahu rank 6 atau rank 7 mau lompat
            {
                if (cek_lompat(pemakan, x, y) == true)
                {
                    return true;
                }
                else
                {
                    message = "Invalid Move";
                }
            }
            else
            {
                message = "Invalid Move";
            }
        }
        else
        {
            message = "Out of Bounds";
        }

        return false;
    }

    private boolean cek_lompat(hewan pemakan, int x, int y) {
        hewan dimakan = cari_hewan_di_posisi(x, y);

        if (pemakan.x == x && (pemakan.y + 4) == y)
        {
            for (int i = 1; i < 4; i++) //cek lompat atas ke bawah
            {
                if (!map[pemakan.y + i][x].equals("A"))
                {
                    return false;
                }
            }

            if (dimakan != null && !pemakan.warna.equals(dimakan.warna) && pemakan.rank >= dimakan.rank)
            {
                return true;
            }
            else if (dimakan == null)
            {
                return true;
            }
        }

        if (pemakan.x == x && (pemakan.y - 4) == y)
        {
            for (int i = 1; i < 4; i++) //cek lompat bawah ke atas
            {
                if (!map[pemakan.y - i][x].equals("A"))
                {
                    return false;
                }
            }

            if (dimakan != null && !pemakan.warna.equals(dimakan.warna) && pemakan.rank >= dimakan.rank)
            {
                return true;
            }
            else if (dimakan == null)
            {
                return true;
            }
        }

        if (pemakan.x + 3 == x && pemakan.y == y) //cek lompat kiri ke kanan
        {
            for (int i = 1; i < 3; i++)
            {
                if (!map[y][pemakan.x + i].equals("A"))
                {
                    return false;
                }
            }

            if (dimakan != null && !pemakan.warna.equals(dimakan.warna) && pemakan.rank >= dimakan.rank)
            {
                return true;
            }
            else if (dimakan == null)
            {
                return true;
            }
        }

        if (pemakan.x - 3 == x && pemakan.y == y) //cek lompat kanan ke kiri
        {
            for (int i = 1; i < 3; i++)
            {
                if (!map[y][pemakan.x - i].equals("A"))
                {
                    return false;
                }
            }

            if (dimakan != null && !pemakan.warna.equals(dimakan.warna) && pemakan.rank >= dimakan.rank)
            {
                return true;
            }
            else if (dimakan == null)
            {
                return true;
            }
        }

        return false;
    }

    public hewan cari_hewan_di_posisi(int x, int y) {
        hewan hewan = null;

        for (int i = 0; i < 8; i++)
        {
            if (list_hewan_player1.get(i).status_hidup == true && list_hewan_player1.get(i).x == x && list_hewan_player1.get(i).y == y)
            {
                hewan = list_hewan_player1.get(i);
                break;
            }

            if (list_hewan_player2.get(i).status_hidup == true && list_hewan_player2.get(i).x == x && list_hewan_player2.get(i).y == y)
            {
                hewan = list_hewan_player2.get(i);
                break;
            }
        }

        return hewan;
    }

    public boolean cek_menang() {
        if (!map[0][3].equals("RG")) //jika biru ke goal merah
        {
            message = "Player 1 Win";
            return true;
        }

        if (!map[8][3].equals("BG")) //jika merah ke goal biru
        {
            message = "Player 2 Win";
            return true;
        }

        //cek hewan habis
        int CTR_hewan_player1 = 8;
        int CTR_hewan_player2 = 8;

        for (int i = 0; i < 8; i++)
        {
            if (list_hewan_player1.get(i).status_hidup == false)
            {
                CTR_hewan_player1--;
            }

            if (list_hewan_player2.get(i).status_hidup == false)
            {
                CTR_hewan_player2--;
            }
        }

        if (CTR_hewan_player2 <= 0)
        {
            message = "Player 1 Win";
            return true;
        }

        if (CTR_hewan_player1 <= 0)
        {
            message = "Player 2 Win";
            return true;
        }

        return false;
    }

    public int hitung_sbe(int[] hewan_value, int[][][] hewan_map_value) {
        int sbe = 0;

        for (int i = 0; i < 8; i++)
        {
            if (list_hewan_player2.get(i).status_hidup == true)
            {
                sbe += hewan_value[i] + hewan_map_value[i][list_hewan_player2.get(i).y][list_hewan_player2.get(i).x];
            }

            if (list_hewan_player1.get(i).status_hidup == true)
            {
                sbe -= hewan_value[i] + hewan_map_value[i][8 - list_hewan_player1.get(i).y][6 - list_hewan_player1.get(i).x];
            }
        }

        return sbe;
    }
}
