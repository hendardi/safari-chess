package com.example.aiproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Random;

public class PlayActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout container;
    state state;
    String mode;
    boolean klik_1;
    hewan pemakan;
    int ply = 5;
    //searah jarum jam dari atas
    int[] arr_gerak_x = {0, 3, 0, -3, 0, 1, 0, -1};
    int[] arr_gerak_y = {-4, 0, 4, 0, -1, 0, 1, 0};
    int simpan_hewan = 0;
    int simpan_gerak = 0;
    int alpha = -99999;
    int beta = 99999;
    int[] hewan_value = new int[8];
    int[][][] hewan_map_value = new int[8][9][7];
    String[][] terrain = new String[9][7];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        container = findViewById(R.id.container);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            mode = bundle.getString("mode");
        }

        //init hewan dan hewan map value
        for (int i = 0; i < hewan_map_value.length; i++)
        {
            if (i == 0)
            {
                //tikus
                hewan_value[i] = 600;
                //baris1
                hewan_map_value[i][8][6] = 11;
                hewan_map_value[i][8][5] = 13;
                hewan_map_value[i][8][4] = 50;
                hewan_map_value[i][8][3] = 99999;
                hewan_map_value[i][8][2] = 50;
                hewan_map_value[i][8][1] = 13;
                hewan_map_value[i][8][0] = 13;
                // baris2
                hewan_map_value[i][7][6] = 11;
                hewan_map_value[i][7][5] = 12;
                hewan_map_value[i][7][4] = 13;
                hewan_map_value[i][7][3] = 50;
                hewan_map_value[i][7][2] = 13;
                hewan_map_value[i][7][1] = 13;
                hewan_map_value[i][7][0] = 13;
                //baris3
                hewan_map_value[i][6][6] = 10;
                hewan_map_value[i][6][5] = 11;
                hewan_map_value[i][6][4] = 11;
                hewan_map_value[i][6][3] = 13;
                hewan_map_value[i][6][2] = 13;
                hewan_map_value[i][6][1] = 15;
                hewan_map_value[i][6][0] = 16;
                //baris4
                hewan_map_value[i][5][6] = 8;
                hewan_map_value[i][5][5] = 9;
                hewan_map_value[i][5][4] = 9;
                hewan_map_value[i][5][3] = 11;
                hewan_map_value[i][5][2] = 12;
                hewan_map_value[i][5][1] = 12;
                hewan_map_value[i][5][0] = 15;
                //baris5
                hewan_map_value[i][4][6] = 8;
                hewan_map_value[i][4][5] = 9;
                hewan_map_value[i][4][4] = 9;
                hewan_map_value[i][4][3] = 11;
                hewan_map_value[i][4][2] = 12;
                hewan_map_value[i][4][1] = 12;
                hewan_map_value[i][4][0] = 14;
                //baris6
                hewan_map_value[i][3][6] = 8;
                hewan_map_value[i][3][5] = 9;
                hewan_map_value[i][3][4] = 9;
                hewan_map_value[i][3][3] = 10;
                hewan_map_value[i][3][2] = 12;
                hewan_map_value[i][3][1] = 12;
                hewan_map_value[i][3][0] = 12;
                //baris7
                hewan_map_value[i][2][6] = 8;
                hewan_map_value[i][2][5] = 8;
                hewan_map_value[i][2][4] = 8;
                hewan_map_value[i][2][3] = 9;
                hewan_map_value[i][2][2] = 10;
                hewan_map_value[i][2][1] = 10;
                hewan_map_value[i][2][0] = 10;
                //baris8
                hewan_map_value[i][1][6] = 8;
                hewan_map_value[i][1][5] = 8;
                hewan_map_value[i][1][4] = 8;
                hewan_map_value[i][1][3] = 9;
                hewan_map_value[i][1][2] = 9;
                hewan_map_value[i][1][1] = 9;
                hewan_map_value[i][1][0] = 9;
                //baris9
                hewan_map_value[i][0][6] = 8;
                hewan_map_value[i][0][5] = 8;
                hewan_map_value[i][0][4] = 8;
                hewan_map_value[i][0][3] = 0;
                hewan_map_value[i][0][2] = 8;
                hewan_map_value[i][0][1] = 8;
                hewan_map_value[i][0][0] = 8;
            }
            else if (i == 1)
            {
                //kucing
                hewan_value[i] = 200;
                //baris 1
                hewan_map_value[i][8][6] = 11;
                hewan_map_value[i][8][5] = 15;
                hewan_map_value[i][8][4] = 50;
                hewan_map_value[i][8][3] = 99999;
                hewan_map_value[i][8][2] = 50;
                hewan_map_value[i][8][1] = 15;
                hewan_map_value[i][8][0] = 11;
                //baris 2
                hewan_map_value[i][7][6] = 11;
                hewan_map_value[i][7][5] = 11;
                hewan_map_value[i][7][4] = 15;
                hewan_map_value[i][7][3] = 50;
                hewan_map_value[i][7][2] = 15;
                hewan_map_value[i][7][1] = 11;
                hewan_map_value[i][7][0] = 11;
                //baris 3
                hewan_map_value[i][6][6] = 10;
                hewan_map_value[i][6][5] = 11;
                hewan_map_value[i][6][4] = 11;
                hewan_map_value[i][6][3] = 15;
                hewan_map_value[i][6][2] = 11;
                hewan_map_value[i][6][1] = 11;
                hewan_map_value[i][6][0] = 10;
                //baris 4
                hewan_map_value[i][5][6] = 10;
                hewan_map_value[i][5][5] = 0;
                hewan_map_value[i][5][4] = 0;
                hewan_map_value[i][5][3] = 10;
                hewan_map_value[i][5][2] = 0;
                hewan_map_value[i][5][1] = 0;
                hewan_map_value[i][5][0] = 8;
                //baris 5
                hewan_map_value[i][4][6] = 10;
                hewan_map_value[i][4][5] = 0;
                hewan_map_value[i][4][4] = 0;
                hewan_map_value[i][4][3] = 8;
                hewan_map_value[i][4][2] = 0;
                hewan_map_value[i][4][1] = 0;
                hewan_map_value[i][4][0] = 8;
                //baris 6
                hewan_map_value[i][3][6] = 10;
                hewan_map_value[i][3][5] = 0;
                hewan_map_value[i][3][4] = 0;
                hewan_map_value[i][3][3] = 8;
                hewan_map_value[i][3][2] = 0;
                hewan_map_value[i][3][1] = 0;
                hewan_map_value[i][3][0] = 8;
                //baris 7
                hewan_map_value[i][2][6] = 10;
                hewan_map_value[i][2][5] = 10;
                hewan_map_value[i][2][4] = 10;
                hewan_map_value[i][2][3] = 8;
                hewan_map_value[i][2][2] = 8;
                hewan_map_value[i][2][1] = 8;
                hewan_map_value[i][2][0] = 8;
                //baris 8
                hewan_map_value[i][1][6] = 13;
                hewan_map_value[i][1][5] = 10;
                hewan_map_value[i][1][4] = 8;
                hewan_map_value[i][1][3] = 8;
                hewan_map_value[i][1][2] = 8;
                hewan_map_value[i][1][1] = 8;
                hewan_map_value[i][1][0] = 8;
                //baris 9
                hewan_map_value[i][0][6] = 8;
                hewan_map_value[i][0][5] = 8;
                hewan_map_value[i][0][4] = 8;
                hewan_map_value[i][0][3] = 0;
                hewan_map_value[i][0][2] = 8;
                hewan_map_value[i][0][1] = 8;
                hewan_map_value[i][0][0] = 8;
            }
            else if (i == 2)
            {
                //anjing
                hewan_value[i] = 300;
                //baris1
                hewan_map_value[i][8][6] = 11;
                hewan_map_value[i][8][5] = 15;
                hewan_map_value[i][8][4] = 50;
                hewan_map_value[i][8][3] = 99999;
                hewan_map_value[i][8][2] = 50;
                hewan_map_value[i][8][1] = 15;
                hewan_map_value[i][8][0] = 11;
                // baris2
                hewan_map_value[i][7][6] = 10;
                hewan_map_value[i][7][5] = 11;
                hewan_map_value[i][7][4] = 15;
                hewan_map_value[i][7][3] = 50;
                hewan_map_value[i][7][2] = 15;
                hewan_map_value[i][7][1] = 11;
                hewan_map_value[i][7][0] = 10;
                //baris3
                hewan_map_value[i][6][6] = 9;
                hewan_map_value[i][6][5] = 10;
                hewan_map_value[i][6][4] = 11;
                hewan_map_value[i][6][3] = 15;
                hewan_map_value[i][6][2] = 11;
                hewan_map_value[i][6][1] = 10;
                hewan_map_value[i][6][0] = 9;
                //baris4
                hewan_map_value[i][5][6] = 9;
                hewan_map_value[i][5][5] = 0;
                hewan_map_value[i][5][4] = 0;
                hewan_map_value[i][5][3] = 10;
                hewan_map_value[i][5][2] = 0;
                hewan_map_value[i][5][1] = 0;
                hewan_map_value[i][5][0] = 9;
                //baris5
                hewan_map_value[i][4][6] = 8;
                hewan_map_value[i][4][5] = 0;
                hewan_map_value[i][4][4] = 0;
                hewan_map_value[i][4][3] = 8;
                hewan_map_value[i][4][2] = 0;
                hewan_map_value[i][4][1] = 0;
                hewan_map_value[i][4][0] = 8;
                //baris6
                hewan_map_value[i][3][6] = 8;
                hewan_map_value[i][3][5] = 0;
                hewan_map_value[i][3][4] = 0;
                hewan_map_value[i][3][3] = 8;
                hewan_map_value[i][3][2] = 0;
                hewan_map_value[i][3][1] = 0;
                hewan_map_value[i][3][0] = 8;
                //baris7
                hewan_map_value[i][2][6] = 8;
                hewan_map_value[i][2][5] = 8;
                hewan_map_value[i][2][4] = 8;
                hewan_map_value[i][2][3] = 8;
                hewan_map_value[i][2][2] = 8;
                hewan_map_value[i][2][1] = 8;
                hewan_map_value[i][2][0] = 8;
                //baris8
                hewan_map_value[i][1][6] = 8;
                hewan_map_value[i][1][5] = 8;
                hewan_map_value[i][1][4] = 8;
                hewan_map_value[i][1][3] = 8;
                hewan_map_value[i][1][2] = 13;
                hewan_map_value[i][1][1] = 10;
                hewan_map_value[i][1][0] = 8;
                //baris9
                hewan_map_value[i][0][6] = 8;
                hewan_map_value[i][0][5] = 8;
                hewan_map_value[i][0][4] = 8;
                hewan_map_value[i][0][3] = 0;
                hewan_map_value[i][0][2] = 12;
                hewan_map_value[i][0][1] = 12;
                hewan_map_value[i][0][0] = 8;
            }
            else if (i == 3)
            {
                //serigala
                hewan_value[i] = 400;
                //baris 1
                hewan_map_value[i][8][6] = 11;
                hewan_map_value[i][8][5] = 15;
                hewan_map_value[i][8][4] = 50;
                hewan_map_value[i][8][3] = 99999;
                hewan_map_value[i][8][2] = 50;
                hewan_map_value[i][8][1] = 15;
                hewan_map_value[i][8][0] = 11;
                //baris 2
                hewan_map_value[i][7][6] = 10;
                hewan_map_value[i][7][5] = 11;
                hewan_map_value[i][7][4] = 15;
                hewan_map_value[i][7][3] = 50;
                hewan_map_value[i][7][2] = 15;
                hewan_map_value[i][7][1] = 11;
                hewan_map_value[i][7][0] = 10;
                //baris 3
                hewan_map_value[i][6][6] = 9;
                hewan_map_value[i][6][5] = 10;
                hewan_map_value[i][6][4] = 11;
                hewan_map_value[i][6][3] = 15;
                hewan_map_value[i][6][2] = 11;
                hewan_map_value[i][6][1] = 10;
                hewan_map_value[i][6][0] = 9;
                //baris 4
                hewan_map_value[i][5][6] = 9;
                hewan_map_value[i][5][5] = 0;
                hewan_map_value[i][5][4] = 0;
                hewan_map_value[i][5][3] = 10;
                hewan_map_value[i][5][2] = 0;
                hewan_map_value[i][5][1] = 0;
                hewan_map_value[i][5][0] = 9;
                //baris 5
                hewan_map_value[i][4][6] = 8;
                hewan_map_value[i][4][5] = 0;
                hewan_map_value[i][4][4] = 0;
                hewan_map_value[i][4][3] = 8;
                hewan_map_value[i][4][2] = 0;
                hewan_map_value[i][4][1] = 0;
                hewan_map_value[i][4][0] = 8;
                //baris 6
                hewan_map_value[i][3][6] = 8;
                hewan_map_value[i][3][5] = 0;
                hewan_map_value[i][3][4] = 0;
                hewan_map_value[i][3][3] = 8;
                hewan_map_value[i][3][2] = 0;
                hewan_map_value[i][3][1] = 0;
                hewan_map_value[i][3][0] = 8;
                //baris 7
                hewan_map_value[i][2][6] = 8;
                hewan_map_value[i][2][5] = 8;
                hewan_map_value[i][2][4] = 10;
                hewan_map_value[i][2][3] = 8;
                hewan_map_value[i][2][2] = 8;
                hewan_map_value[i][2][1] = 8;
                hewan_map_value[i][2][0] = 8;
                //baris 8
                hewan_map_value[i][1][6] = 8;
                hewan_map_value[i][1][5] = 12;
                hewan_map_value[i][1][4] = 13;
                hewan_map_value[i][1][3] = 8;
                hewan_map_value[i][1][2] = 8;
                hewan_map_value[i][1][1] = 8;
                hewan_map_value[i][1][0] = 8;
                //baris 9
                hewan_map_value[i][0][6] = 8;
                hewan_map_value[i][0][5] = 12;
                hewan_map_value[i][0][4] = 12;
                hewan_map_value[i][0][3] = 0;
                hewan_map_value[i][0][2] = 8;
                hewan_map_value[i][0][1] = 8;
                hewan_map_value[i][0][0] = 8;
            }
            else if (i == 4)
            {
                //ceetah
                hewan_value[i] = 550;
                //baris1
                hewan_map_value[i][8][6] = 14;
                hewan_map_value[i][8][5] = 15;
                hewan_map_value[i][8][4] = 50;
                hewan_map_value[i][8][3] = 99999;
                hewan_map_value[i][8][2] = 50;
                hewan_map_value[i][8][1] = 15;
                hewan_map_value[i][8][0] = 14;
                // baris2
                hewan_map_value[i][7][6] = 13;
                hewan_map_value[i][7][5] = 14;
                hewan_map_value[i][7][4] = 15;
                hewan_map_value[i][7][3] = 50;
                hewan_map_value[i][7][2] = 15;
                hewan_map_value[i][7][1] = 14;
                hewan_map_value[i][7][0] = 13;
                //baris3
                hewan_map_value[i][6][6] = 13;
                hewan_map_value[i][6][5] = 13;
                hewan_map_value[i][6][4] = 14;
                hewan_map_value[i][6][3] = 15;
                hewan_map_value[i][6][2] = 14;
                hewan_map_value[i][6][1] = 13;
                hewan_map_value[i][6][0] = 13;
                //baris4
                hewan_map_value[i][5][6] = 12;
                hewan_map_value[i][5][5] = 0;
                hewan_map_value[i][5][4] = 0;
                hewan_map_value[i][5][3] = 14;
                hewan_map_value[i][5][2] = 0;
                hewan_map_value[i][5][1] = 0;
                hewan_map_value[i][5][0] = 12;
                //baris5
                hewan_map_value[i][4][6] = 11;
                hewan_map_value[i][4][5] = 0;
                hewan_map_value[i][4][4] = 0;
                hewan_map_value[i][4][3] = 13;
                hewan_map_value[i][4][2] = 0;
                hewan_map_value[i][4][1] = 0;
                hewan_map_value[i][4][0] = 11;
                //baris6
                hewan_map_value[i][3][6] = 10;
                hewan_map_value[i][3][5] = 0;
                hewan_map_value[i][3][4] = 0;
                hewan_map_value[i][3][3] = 12;
                hewan_map_value[i][3][2] = 0;
                hewan_map_value[i][3][1] = 0;
                hewan_map_value[i][3][0] = 10;
                //baris7
                hewan_map_value[i][2][6] = 9;
                hewan_map_value[i][2][5] = 9;
                hewan_map_value[i][2][4] = 9;
                hewan_map_value[i][2][3] = 10;
                hewan_map_value[i][2][2] = 10;
                hewan_map_value[i][2][1] = 9;
                hewan_map_value[i][2][0] = 9;
                //baris8
                hewan_map_value[i][1][6] = 9;
                hewan_map_value[i][1][5] = 9;
                hewan_map_value[i][1][4] = 9;
                hewan_map_value[i][1][3] = 9;
                hewan_map_value[i][1][2] = 9;
                hewan_map_value[i][1][1] = 9;
                hewan_map_value[i][1][0] = 9;
                //baris9
                hewan_map_value[i][0][6] = 9;
                hewan_map_value[i][0][5] = 9;
                hewan_map_value[i][0][4] = 9;
                hewan_map_value[i][0][3] = 0;
                hewan_map_value[i][0][2] = 9;
                hewan_map_value[i][0][1] = 9;
                hewan_map_value[i][0][0] = 9;
            }
            else if (i == 5)
            {
                //harimau
                hewan_value[i] = 800;
                //baris 1
                hewan_map_value[i][8][6] = 25;
                hewan_map_value[i][8][5] = 30;
                hewan_map_value[i][8][4] = 50;
                hewan_map_value[i][8][3] = 99999;
                hewan_map_value[i][8][2] = 50;
                hewan_map_value[i][8][1] = 30;
                hewan_map_value[i][8][0] = 25;
                //baris 2
                hewan_map_value[i][7][6] = 25;
                hewan_map_value[i][7][5] = 25;
                hewan_map_value[i][7][4] = 30;
                hewan_map_value[i][7][3] = 50;
                hewan_map_value[i][7][2] = 30;
                hewan_map_value[i][7][1] = 25;
                hewan_map_value[i][7][0] = 25;
                //baris 3
                hewan_map_value[i][6][6] = 18;
                hewan_map_value[i][6][5] = 20;
                hewan_map_value[i][6][4] = 20;
                hewan_map_value[i][6][3] = 30;
                hewan_map_value[i][6][2] = 20;
                hewan_map_value[i][6][1] = 20;
                hewan_map_value[i][6][0] = 18;
                //baris 4
                hewan_map_value[i][5][6] = 19;
                hewan_map_value[i][5][5] = 0;
                hewan_map_value[i][5][4] = 0;
                hewan_map_value[i][5][3] = 19;
                hewan_map_value[i][5][2] = 0;
                hewan_map_value[i][5][1] = 0;
                hewan_map_value[i][5][0] = 19;
                //baris 5
                hewan_map_value[i][4][6] = 18;
                hewan_map_value[i][4][5] = 0;
                hewan_map_value[i][4][4] = 0;
                hewan_map_value[i][4][3] = 18;
                hewan_map_value[i][4][2] = 0;
                hewan_map_value[i][4][1] = 0;
                hewan_map_value[i][4][0] = 18;
                //baris 6
                hewan_map_value[i][3][6] = 17;
                hewan_map_value[i][3][5] = 0;
                hewan_map_value[i][3][4] = 0;
                hewan_map_value[i][3][3] = 17;
                hewan_map_value[i][3][2] = 0;
                hewan_map_value[i][3][1] = 0;
                hewan_map_value[i][3][0] = 17;
                //baris 7
                hewan_map_value[i][2][6] = 16;
                hewan_map_value[i][2][5] = 16;
                hewan_map_value[i][2][4] = 16;
                hewan_map_value[i][2][3] = 16;
                hewan_map_value[i][2][2] = 16;
                hewan_map_value[i][2][1] = 16;
                hewan_map_value[i][2][0] = 16;
                //baris 8
                hewan_map_value[i][1][6] = 12;
                hewan_map_value[i][1][5] = 14;
                hewan_map_value[i][1][4] = 12;
                hewan_map_value[i][1][3] = 12;
                hewan_map_value[i][1][2] = 12;
                hewan_map_value[i][1][1] = 12;
                hewan_map_value[i][1][0] = 12;
                //baris 9
                hewan_map_value[i][0][6] = 10;
                hewan_map_value[i][0][5] = 13;
                hewan_map_value[i][0][4] = 12;
                hewan_map_value[i][0][3] = 0;
                hewan_map_value[i][0][2] = 12;
                hewan_map_value[i][0][1] = 12;
                hewan_map_value[i][0][0] = 10;
            }
            else if (i == 6)
            {
                //singa
                hewan_value[i] = 900;
                //baris1
                hewan_map_value[i][8][6] = 25;
                hewan_map_value[i][8][5] = 30;
                hewan_map_value[i][8][4] = 50;
                hewan_map_value[i][8][3] = 99999;
                hewan_map_value[i][8][2] = 50;
                hewan_map_value[i][8][1] = 30;
                hewan_map_value[i][8][0] = 25;
                // baris2
                hewan_map_value[i][7][6] = 25;
                hewan_map_value[i][7][5] = 25;
                hewan_map_value[i][7][4] = 30;
                hewan_map_value[i][7][3] = 50;
                hewan_map_value[i][7][2] = 30;
                hewan_map_value[i][7][1] = 25;
                hewan_map_value[i][7][0] = 25;
                //baris3
                hewan_map_value[i][6][6] = 18;
                hewan_map_value[i][6][5] = 20;
                hewan_map_value[i][6][4] = 20;
                hewan_map_value[i][6][3] = 30;
                hewan_map_value[i][6][2] = 20;
                hewan_map_value[i][6][1] = 20;
                hewan_map_value[i][6][0] = 18;
                //baris4
                hewan_map_value[i][5][6] = 19;
                hewan_map_value[i][5][5] = 0;
                hewan_map_value[i][5][4] = 0;
                hewan_map_value[i][5][3] = 19;
                hewan_map_value[i][5][2] = 0;
                hewan_map_value[i][5][1] = 0;
                hewan_map_value[i][5][0] = 19;
                //baris5
                hewan_map_value[i][4][6] = 18;
                hewan_map_value[i][4][5] = 0;
                hewan_map_value[i][4][4] = 0;
                hewan_map_value[i][4][3] = 18;
                hewan_map_value[i][4][2] = 0;
                hewan_map_value[i][4][1] = 0;
                hewan_map_value[i][4][0] = 18;
                //baris6
                hewan_map_value[i][3][6] = 17;
                hewan_map_value[i][3][5] = 0;
                hewan_map_value[i][3][4] = 0;
                hewan_map_value[i][3][3] = 17;
                hewan_map_value[i][3][2] = 0;
                hewan_map_value[i][3][1] = 0;
                hewan_map_value[i][3][0] = 17;
                //baris7
                hewan_map_value[i][2][6] = 16;
                hewan_map_value[i][2][5] = 16;
                hewan_map_value[i][2][4] = 16;
                hewan_map_value[i][2][3] = 16;
                hewan_map_value[i][2][2] = 16;
                hewan_map_value[i][2][1] = 16;
                hewan_map_value[i][2][0] = 16;
                //baris8
                hewan_map_value[i][1][6] = 12;
                hewan_map_value[i][1][5] = 12;
                hewan_map_value[i][1][4] = 12;
                hewan_map_value[i][1][3] = 12;
                hewan_map_value[i][1][2] = 12;
                hewan_map_value[i][1][1] = 14;
                hewan_map_value[i][1][0] = 12;
                //baris9
                hewan_map_value[i][0][6] = 10;
                hewan_map_value[i][0][5] = 12;
                hewan_map_value[i][0][4] = 12;
                hewan_map_value[i][0][3] = 0;
                hewan_map_value[i][0][2] = 12;
                hewan_map_value[i][0][1] = 13;
                hewan_map_value[i][0][0] = 10;
            }
            else if (i == 7)
            {
                //gajah
                hewan_value[i] = 1000;
                //baris 1
                hewan_map_value[i][8][6] = 25;
                hewan_map_value[i][8][5] = 30;
                hewan_map_value[i][8][4] = 50;
                hewan_map_value[i][8][3] = 99999;
                hewan_map_value[i][8][2] = 50;
                hewan_map_value[i][8][1] = 30;
                hewan_map_value[i][8][0] = 25;
                //baris 2
                hewan_map_value[i][7][6] = 25;
                hewan_map_value[i][7][5] = 25;
                hewan_map_value[i][7][4] = 30;
                hewan_map_value[i][7][3] = 50;
                hewan_map_value[i][7][2] = 30;
                hewan_map_value[i][7][1] = 25;
                hewan_map_value[i][7][0] = 25;
                //baris 3
                hewan_map_value[i][6][6] = 18;
                hewan_map_value[i][6][5] = 20;
                hewan_map_value[i][6][4] = 20;
                hewan_map_value[i][6][3] = 30;
                hewan_map_value[i][6][2] = 20;
                hewan_map_value[i][6][1] = 20;
                hewan_map_value[i][6][0] = 18;
                //baris 4
                hewan_map_value[i][5][6] = 16;
                hewan_map_value[i][5][5] = 0;
                hewan_map_value[i][5][4] = 0;
                hewan_map_value[i][5][3] = 16;
                hewan_map_value[i][5][2] = 0;
                hewan_map_value[i][5][1] = 0;
                hewan_map_value[i][5][0] = 16;
                //baris 5
                hewan_map_value[i][4][6] = 14;
                hewan_map_value[i][4][5] = 0;
                hewan_map_value[i][4][4] = 0;
                hewan_map_value[i][4][3] = 14;
                hewan_map_value[i][4][2] = 0;
                hewan_map_value[i][4][1] = 0;
                hewan_map_value[i][4][0] = 14;
                //baris 6
                hewan_map_value[i][3][6] = 12;
                hewan_map_value[i][3][5] = 0;
                hewan_map_value[i][3][4] = 0;
                hewan_map_value[i][3][3] = 12;
                hewan_map_value[i][3][2] = 0;
                hewan_map_value[i][3][1] = 0;
                hewan_map_value[i][3][0] = 12;
                //baris 7
                hewan_map_value[i][2][6] = 10;
                hewan_map_value[i][2][5] = 14;
                hewan_map_value[i][2][4] = 15;
                hewan_map_value[i][2][3] = 14;
                hewan_map_value[i][2][2] = 14;
                hewan_map_value[i][2][1] = 14;
                hewan_map_value[i][2][0] = 12;
                //baris 8
                hewan_map_value[i][1][6] = 11;
                hewan_map_value[i][1][5] = 11;
                hewan_map_value[i][1][4] = 11;
                hewan_map_value[i][1][3] = 11;
                hewan_map_value[i][1][2] = 11;
                hewan_map_value[i][1][1] = 11;
                hewan_map_value[i][1][0] = 11;
                //baris 9
                hewan_map_value[i][0][6] = 11;
                hewan_map_value[i][0][5] = 11;
                hewan_map_value[i][0][4] = 11;
                hewan_map_value[i][0][3] = 0;
                hewan_map_value[i][0][2] = 11;
                hewan_map_value[i][0][1] = 11;
                hewan_map_value[i][0][0] = 11;
            }
        }

        //init terrain
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                terrain[i][j] = "T";
            }
        }

        terrain[8][3] = "BG";
        terrain[8][2] = "BO";
        terrain[8][4] = "BO";
        terrain[7][3] = "BO";

        terrain[0][3] = "RG";
        terrain[0][2] = "RO";
        terrain[0][4] = "RO";
        terrain[1][3] = "RO";

        terrain[3][1] = "A";
        terrain[3][2] = "A";
        terrain[4][1] = "A";
        terrain[4][2] = "A";
        terrain[5][1] = "A";
        terrain[5][2] = "A";

        terrain[3][4] = "A";
        terrain[3][5] = "A";
        terrain[4][4] = "A";
        terrain[4][5] = "A";
        terrain[5][4] = "A";
        terrain[5][5] = "A";

        init_game();
    }

    private void init_game() {
        Random rand = new Random();
        state = new state(rand.nextBoolean()); //random turn player
        if (mode.equals("2p"))
        {
            if (state.giliran_player_1 == true)
            {
                Toast.makeText(this, "Player 1 Turn", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this, "Player 2 Turn", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            if (state.giliran_player_1 == false)
            {
                //gerak ai
                alpha = -99999;
                beta = 99999;
                gerak_ai(0, state, alpha, beta);
                state.gerakkan_hewan_ke(state.list_hewan_player2.get(simpan_hewan), state.list_hewan_player2.get(simpan_hewan).x + arr_gerak_x[simpan_gerak], state.list_hewan_player2.get(simpan_hewan).y + arr_gerak_y[simpan_gerak], terrain);
                refresh_layout();
            }
        }

        klik_1 = true;
        refresh_layout();
    }

    private void refresh_layout() {
        container.removeAllViews();

        //buat layout
        for (int i = 0; i < 9; i++)
        {
            LinearLayout content = new LinearLayout(this);
            LinearLayout.LayoutParams content_layout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            content.setLayoutParams(content_layout);
            content.setOrientation(LinearLayout.HORIZONTAL);

            for (int j = 0; j < 7; j++)
            {
                ImageView iv = new ImageView(this);
                iv.setAdjustViewBounds(true);
                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                iv.setTag(state.map[i][j] + ";" + j + "," + i);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(150, 150);
                iv.setLayoutParams(layoutParams);
                //iv.setBackgroundResource(R.drawable.border);
                try {
                    Resources res = getResources();
                    String mDrawableName = state.map[i][j].toLowerCase();
                    int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                    iv.setImageResource(resID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                iv.setOnClickListener(this);
                content.addView(iv);
            }

            container.addView(content);
        }
    }

    private int gerak_ai(int kedalaman, state state, int alpha, int beta) {
        if (kedalaman >= ply || state.cek_menang() == true)
        {
            return state.hitung_sbe(hewan_value, hewan_map_value);
        }
        else
        {
            if (kedalaman % 2 == 0) //giliran ai
            {
                int batas_bawah = new Integer(Integer.MIN_VALUE);

                for (int i = 7; i >= 0; i--)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        if (state.list_hewan_player2.get(i).status_hidup == true && state.cek_valid_gerakkan_hewan_ke(state.list_hewan_player2.get(i), state.list_hewan_player2.get(i).x + arr_gerak_x[j], state.list_hewan_player2.get(i).y + arr_gerak_y[j], terrain))
                        {
                            //jika valid clone lalu gerakkan
                            state clone = new state(state);
                            Integer pointer = new Integer(kedalaman + 1);
                            clone.gerakkan_hewan_ke(clone.list_hewan_player2.get(i), clone.list_hewan_player2.get(i).x + arr_gerak_x[j], clone.list_hewan_player2.get(i).y + arr_gerak_y[j], terrain);

                            //simpan sbe hasil backtrack untuk dibandingkan
                            int temp = gerak_ai(pointer, clone, alpha, beta);

                            if (batas_bawah < temp)
                            {
                                if (kedalaman == 0)
                                {
                                    simpan_hewan = i;
                                    simpan_gerak = j;
                                }
                                batas_bawah = temp;
                            }

                            if (alpha < batas_bawah)
                            {
                                alpha = batas_bawah;
                            }

                            if (beta <= alpha)
                            {
                                break;
                            }
                        }
                    }
                }

                return batas_bawah;
            }
            else //giliran player
            {
                int batas_atas = new Integer(Integer.MAX_VALUE);

                for (int i = 7; i >= 0; i--)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        if (state.list_hewan_player1.get(i).status_hidup == true && state.cek_valid_gerakkan_hewan_ke(state.list_hewan_player1.get(i), state.list_hewan_player1.get(i).x + arr_gerak_x[j], state.list_hewan_player1.get(i).y + arr_gerak_y[j], terrain))
                        {
                            //jika valid clone lalu gerakkan
                            state clone = new state(state);
                            Integer pointer = new Integer(kedalaman + 1);
                            clone.gerakkan_hewan_ke(clone.list_hewan_player1.get(i), clone.list_hewan_player1.get(i).x + arr_gerak_x[j], clone.list_hewan_player1.get(i).y + arr_gerak_y[j], terrain);

                            //simpan sbe hasil backtrack untuk dibandingkan
                            int temp = gerak_ai(pointer, clone, alpha, beta);

                            if (batas_atas > temp)
                            {
                                batas_atas = temp;
                            }

                            if (beta > batas_atas)
                            {
                                beta = batas_atas;
                            }

                            if (beta <= alpha)
                            {
                                break;
                            }
                        }
                    }
                }

                return batas_atas;
            }
        }
    }

    @Override
    public void onClick(View v) {
        String split[] = v.getTag().toString().split(";");
        String posisi[] = split[1].split(",");
        int x = Integer.valueOf(posisi[0]);
        int y = Integer.valueOf(posisi[1]);

        if (mode.equals("2p")) //mode vs 2 player
        {
            if (klik_1 == true)
            {
                //cari hewan pemakan
                pemakan = state.cari_hewan_di_posisi(x, y);

                if (state.giliran_player_1 == true)
                {
                    if (pemakan != null) //jika pemakan ditemukan siap siap untuk digerakkan
                    {
                        if (pemakan.warna.equals("B"))
                        {
                            klik_1 = false;
                        }
                        else if (pemakan.warna.equals("R"))
                        {
                            Toast.makeText(this, "Player 1 Turn", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else
                {
                    if (pemakan != null) //jika pemakan ditemukan siap siap untuk digerakkan
                    {
                        if (pemakan.warna.equals("R"))
                        {
                            klik_1 = false;
                        }
                        else if (pemakan.warna.equals("B"))
                        {
                            Toast.makeText(this, "Player 2 Turn", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
            else if (klik_1 == false)
            {
                if (state.cek_valid_gerakkan_hewan_ke(pemakan, x, y, terrain) == true)
                {
                    state.gerakkan_hewan_ke(pemakan, x, y, terrain);

                    if (state.giliran_player_1 == true)
                    {
                        state.giliran_player_1 = false;
                        Toast.makeText(this, "Player 2 Turn", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        state.giliran_player_1 = true;
                        Toast.makeText(this, "Player 1 Turn", Toast.LENGTH_SHORT).show();
                    }

                    //cek menang
                    if (state.cek_menang() == true)
                    {
                        Toast.makeText(this, state.message, Toast.LENGTH_SHORT).show();
                        init_game();
                    }
                }
                else
                {
                    Toast.makeText(this, state.message, Toast.LENGTH_SHORT).show();
                    state.message = "";
                }

                klik_1 = true;
                refresh_layout();
            }
        }
        else //mode vs ai
        {
            if (klik_1 == true)
            {
                //cari hewan pemakan
                pemakan = state.cari_hewan_di_posisi(x, y);

                if (pemakan != null) //jika pemakan ditemukan siap siap untuk digerakkan
                {
                    if (pemakan.warna.equals("B"))
                    {
                        klik_1 = false;
                    }
                    else if (pemakan.warna.equals("R"))
                    {
                        Toast.makeText(this, "Player 1 Turn", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            else if (klik_1 == false)
            {
                if (state.cek_valid_gerakkan_hewan_ke(pemakan, x, y, terrain) == true)
                {
                    state.gerakkan_hewan_ke(pemakan, x, y, terrain);
                    refresh_layout();

                    //cek menang
                    if (state.cek_menang() == false) //jika player belum menang
                    {
                        //gerak ai
                        alpha = -99999;
                        beta = 99999;
                        gerak_ai(0, state, alpha, beta);
                        state.gerakkan_hewan_ke(state.list_hewan_player2.get(simpan_hewan), state.list_hewan_player2.get(simpan_hewan).x + arr_gerak_x[simpan_gerak], state.list_hewan_player2.get(simpan_hewan).y + arr_gerak_y[simpan_gerak], terrain);
                        refresh_layout();

                        if (state.cek_menang() == true) //apakah ai menang
                        {
                            Toast.makeText(this, state.message, Toast.LENGTH_SHORT).show();
                            init_game();
                        }
                    }
                    else
                    {
                        Toast.makeText(this, state.message, Toast.LENGTH_SHORT).show();
                        init_game();
                    }
                }
                else
                {
                    Toast.makeText(this, state.message, Toast.LENGTH_SHORT).show();
                    state.message = "";
                }

                klik_1 = true;
            }
        }
    }
}
