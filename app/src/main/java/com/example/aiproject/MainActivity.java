package com.example.aiproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button button_vs_ai;
    Button button_vs_2p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_vs_ai = findViewById(R.id.button_vs_ai);
        button_vs_2p = findViewById(R.id.button_vs_2p);

        button_vs_ai.setOnClickListener(this);
        button_vs_2p.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent_to_play_activity = new Intent(getApplicationContext(), PlayActivity.class);
        switch (v.getId())
        {
            case R.id.button_vs_ai:
                intent_to_play_activity.putExtra("mode", "ai");
                break;

            case R.id.button_vs_2p:
                intent_to_play_activity.putExtra("mode", "2p");
                break;
        }
        startActivity(intent_to_play_activity);
    }
}
